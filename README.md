<!--
File: README.md
Written by: Stephen M. Reaves
Created on: Mon, 14 Mar 2022
-->

# OBI

OS Build Installer is a tool to install images created from containers directly
to your hard disk.

## What containers can be installed?

Technically any container, but some won't boot.  You'll need at least a kernel
and a bootloader in the image (and probably an init system).  We only support
containers built by [OS Builder](https://gitlab.com/reavessm/osb).  If you
_really_ need a different container, you can try installing an OSB container
(like [ReavesOS](https://quay.io/reavessm/reavesos)) then reinstalling another
container on top of that, but we can't garauntee any level of success for that
scenario.

## Usage

### Dependencies

Install the dependencies in Fedora by runnings `sudo dnf install
{gpgme,btrfs-progs,device-mapper}-devel`

Make sure podman is running as a service by running `sudo systemctl start
podman`

### Run it

You can start the actual command by running `sudo go run
gitlab.com/reavessm/obi/cmd/tui@latest`

