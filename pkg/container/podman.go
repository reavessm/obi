/**
 * File: podman.go
 * Written by: Stephen M. Reaves
 * Created on: Mon, 07 Mar 2022
 */

package container

import (
	"context"
	"fmt"
	"io"
	"sort"

	"github.com/containers/podman/v4/libpod/define"
	"github.com/containers/podman/v4/pkg/bindings"
	"github.com/containers/podman/v4/pkg/bindings/containers"
	"github.com/containers/podman/v4/pkg/bindings/images"
	"github.com/containers/podman/v4/pkg/domain/entities"
	"github.com/containers/podman/v4/pkg/specgen"
	"gitlab.com/reavessm/obi/pkg/types"
)

const (
	DefaultImage = "docker.io/rapscallionreaves/reavesos"
)

type Podman struct {
	Ctx          context.Context
	RawImage     string
	Container    entities.ContainerCreateResponse
	Image        types.Image
	ImageList    types.ImageList
	ExportWriter io.Writer
	//Images    map[string]bool
}

func New() (*Podman, error) {
	ctx, err := bindings.NewConnection(context.Background(),
		fmt.Sprintf("unix:/run/podman/podman.sock"))
	if err != nil {
		return nil, err
	}

	p := &Podman{
		Ctx:      ctx,
		RawImage: DefaultImage,
	}

	return p, nil
}

func (p *Podman) Search(imgName string) (types.ImageList, error) {
	if imgName != "" {
		p.RawImage = imgName
	}
	searchOpts := &images.SearchOptions{}
	searchOpts = searchOpts.WithListTags(true)
	searchOpts = searchOpts.WithLimit(100) // TODO: ???
	imgs, err := images.Search(p.Ctx, p.RawImage, searchOpts)
	if err != nil {
		return nil, err
	}

	sort.Slice(imgs, func(i, j int) bool {
		return imgs[i].Tag > imgs[j].Tag
	})

	for _, v := range imgs {
		p.ImageList = append(p.ImageList, types.Image{
			Name: v.Name,
			Tag:  v.Tag,
		})
	}
	return p.ImageList, nil
}

func (p *Podman) Pull() error {
	pullOpts := &images.PullOptions{}
	pullOpts = pullOpts.WithQuiet(true)
	//log.Println(p.RawImage)
	_, err := images.Pull(p.Ctx, p.RawImage, pullOpts)

	if err != nil {
		return err
	}

	return nil
}

// ParmRawImage is a getter and a setter in one
func (p *Podman) ParmRawImage(image ...string) string {
	if len(image) > 0 {
		p.RawImage = image[0]
	}

	return p.RawImage
}

func (p *Podman) CreateWithSpec() error {
	s := specgen.NewSpecGenerator(p.RawImage, false)
	s.Terminal = true
	s.Entrypoint = []string{"/bin/sh"}

	r, err := containers.CreateWithSpec(p.Ctx, s, nil)
	if err != nil {
		return err
	}

	p.Container = r

	return nil
}

func (p *Podman) Start() error {
	if err := containers.Start(p.Ctx, p.Container.ID, nil); err != nil {
		return err
	}

	w8opts := &containers.WaitOptions{
		Condition: []define.ContainerStatus{define.ContainerStateRunning},
	}

	if _, err := containers.Wait(p.Ctx, p.Container.ID, w8opts); err != nil {
		return err
	}

	return nil
}

func (p *Podman) Inspect() error {
	inspectionData, err := containers.Inspect(p.Ctx, p.Container.ID, nil)
	if err != nil {
		return err
	}

	p.Image.Name = inspectionData.ImageName

	return nil
}

func (p *Podman) Run(rawImage string) (string, error) {
	//log.Println(rawImage)
	p.RawImage = rawImage
	if err := p.Pull(); err != nil {
		return "", err
	}

	if err := p.CreateWithSpec(); err != nil {
		return "", err
	}

	/*
		if err := p.Start(); err != nil {
			return "", err
		}
	*/

	if err := p.Inspect(); err != nil {
		return "", err
	}

	return p.Image.Name, nil
}

func (p *Podman) Export() error {
	return containers.Export(p.Ctx, p.Container.ID, p.ExportWriter, nil)
}
