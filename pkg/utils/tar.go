/**
 * File: tar.go
 * Written by: Stephen M. Reaves
 * Created on: Mon, 07 Mar 2022
 */

package utils

import (
	"archive/tar"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"regexp"
)

func Untar(tarball string) error {
	reader, err := os.Open(tarball)
	if err != nil {
		return err
	}
	defer reader.Close()

	tarReader := tar.NewReader(reader)
	links := make(map[string]string)

	for {
		header, err := tarReader.Next()
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}

		path := filepath.Join("/mnt/tmp/", header.Name)
		info := header.FileInfo()
		if header.Typeflag == tar.TypeSymlink {
			//links[path] = filepath.Dir(path) + "/" + header.Linkname
			links[path] = header.Linkname
			continue
		}

		if info.IsDir() {
			if err = os.MkdirAll(path, info.Mode()); err != nil {
				return err
			}
			continue
		}

		file, err := os.OpenFile(path, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, info.Mode())
		if err != nil {
			return err
		}

		for {
			_, err := io.CopyN(file, tarReader, 1024)
			if err != nil {
				if err == io.EOF {
					break
				}
				return err
			}
		}
		file.Close()
	}

	for k, v := range links {
		if err := os.Symlink(v, k); err != nil {
			return err
		}
	}
	return nil
}

func CreateTarFile(fileName string) (string, *os.File, error) {
	if err := os.MkdirAll("/mnt/tmp", 0777); err != nil {
		return "", nil, err
	}

	removeSlashes := regexp.MustCompile(`.*[\/]+`)
	removeTag := regexp.MustCompile(`:+.*`)

	tarball := fmt.Sprintf("/mnt/tmp/%s.tar", removeTag.ReplaceAllString(removeSlashes.ReplaceAllString(fileName, ""), ""))
	f, err := os.Create(tarball)
	return tarball, f, err
}
