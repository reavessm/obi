package types

type Image struct {
	Name string
	Tag  string
	Err  error
}

type ImageList []Image

// https://github.com/charmbracelet/bubbletea/blob/master/examples/list-simple/main.go
func (i Image) Title() string       { return i.Tag }
func (i Image) Description() string { return i.Name }
func (i Image) FilterValue() string { return i.Tag }
