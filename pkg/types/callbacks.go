package types

type FetcherFunc func(string) (string, error)
type ListerFunc func(string) (ImageList, error)
