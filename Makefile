default: tui

tui:
	GOOS=linux go build -o obi-tui cmd/obi/main.go

.PHONY: build

clean:
	rm -f obi*

.PHONY: clean
