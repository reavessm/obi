#!/bin/bash

# Written by: Stephen M. Reaves
# Created on: Tue, 22 Feb 2022

echo "MAKEOPTS=\"-j$(nproc)\"" >> /etc/portage/make.conf
echo "EMERGE_DEFAULT_OPTS=\"--ask n --quiet -j $(nproc) -l $(nproc) --autounmask=y --autounmask-continue\"" >> /etc/portage/make.conf
echo 'ACCEPT_KEYWORDS="~amd64"' >> /etc/portage/make.conf
echo 'ACCEPT_LICENSE="@BINARY-REDISTRIBUTABLE"' >> /etc/portage/make.conf
emerge --sync

echo "America/New_York" > /etc/timezone
emerge --config sys-libs/timezone-data

echo 'en_US ISO-8859-1' >> /etc/locale.gen
echo 'en_us.UTF-8 UTF-8' >> /etc/locale.gen
locale-gen

kern_compat="$(awk -F'=' '/ZFS_KERNEL_COMPAT/ {gsub(/"/, ""); print $2; exit}' /var/db/repos/gentoo/sys-fs/zfs-kmod/zfs-kmod-9999.ebuild)"
kern_incompat="$(echo "${kern_compat} + 0.01" | bc)"
echo ">=sys-kernel/gentoo-sources-${kern_incompat}" >> /etc/portage/package.mask

echo 'sys-kernel/gentoo-sources symlink' >> /etc/portage/package.use/kernel
emerge linux-firmware genkernel gentoo-sources sys-apps/pciutils vim

cd /usr/src/linux
#make allmodconfig
#make mrproper defconfig localmodconfig
#sed -i 's/SYSTEMD=y/SYSTEMD=n/' .config
make -j$(nproc) mrproper defconfig localmodconfig all modules_install install

cat /etc/mtab | grep -vE 'zfs|pipefs|proc|udev|devpts|/sys|sysfs|mqueue|/run|none' > /etc/fstab

echo 'hostname="reavesos"' > /etc/conf.d/hostname

export pkgsWithSVC="net-misc/dhcpcd app-admin/sysklogd sys-process/cronie"
export pkgs="${pkgsWithSVC} sys-apps/mlocate sys-fs/e2fsprogs sys-fs/xfsprogs sys-fs/dosfstools sys-fs/btrfs-progs sys-fs/zfs net-wireless/wpa_supplicant"
emerge ${pkgs}

for f in ${pkgsWithSVC}
do
  svc="$(echo "${f}" | awk -F '/' '{print $NF}')"
  rc-update add "${svc}" default
  rc-service "${svc}" start
done

echo 'GRUB_PLATFORMS="efi-64"' >> /etc/portage/make.conf
emerge sys-boot/grub:2
mount -o remount,rw /sys/firmware/efi/efivars
grub-install --target=x64_64-efi --efi-directory=/boot

poweroff
