/**
	"github.com/containers/podman/v4/pkg/specgen"
 * File: main.go
 * Written by: Stephen M. Reaves
 * Created on: Mon, 21 Feb 2022
*/

package main

import (
	"errors"
	"fmt"
	"os"
	"os/user"

	tui "gitlab.com/reavessm/obi/cmd/tui/tea"
	"gitlab.com/reavessm/obi/pkg/container"

	"gitlab.com/reavessm/obi/pkg/utils"
)

func printAndSet(s string) string {
	fmt.Println(s)
	return s
}

func run() error {

	if currentUser, _ := user.Current(); currentUser.Username != "root" {
		return errors.New("Please run with sudo")
	}

	p, err := container.New()
	if err != nil {
		return err
	}
	if p == nil {
		return errors.New("No PP")
	}

	m := tui.NewModel()
	if m == nil {
		return errors.New("No MM")
	}

	m.Lister = p.Search

	m.Fetcher = func(s string) (string, error) {
		imgName, err := p.Run(s)
		if err != nil {
			return "", err
		}

		tarball, containerTar, err := utils.CreateTarFile(imgName)
		if err != nil {
			return "", err
		}

		p.ExportWriter = containerTar
		p.Export()

		err = utils.Untar(tarball)
		return imgName, err
	}

	return tui.ShowAndRun(m)
}

func main() {
	if err := run(); err != nil {
		fmt.Fprintf(os.Stderr, "Error! %v\n", err)
		os.Exit(1)
	}
}
