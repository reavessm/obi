/**
 * File: controller.go
 * Written by: Stephen M. Reaves
 * Created on: Fri, 11 Mar 2022
 */

package tui

import (
	"fmt"
	"os"
	"strings"

	"github.com/charmbracelet/bubbles/list"
	"github.com/charmbracelet/bubbles/spinner"
	tea "github.com/charmbracelet/bubbletea"
	"gitlab.com/reavessm/obi/pkg/types"
	"golang.org/x/term"
)

func (m *Model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch m.state {
	case Typing:
		switch msg := msg.(type) {
		case tea.WindowSizeMsg:
			updateDims(Typing)
		case tea.KeyMsg:
			switch msg.Type {
			case tea.KeyCtrlC:
				return m, tea.Quit
			case tea.KeyEnter:
				img := strings.TrimSpace(m.textInput.Value())
				if img == "" {
					img = m.DefaultImageName
				}
				m.textInput.SetValue("")
				m.state = Listing
				m.RequestedImageName = img

				l, e := m.list(m.RequestedImageName)
				if e != nil {
					m.err = e
					m.state = Results
					updateDims(Results)
					m.textInput.Reset()
					m.textInput.Placeholder = ""
				}

				//m.listModel = list.New(l, list.NewDefaultDelegate(), 0, len(l))
				m.listModel = list.New(l, list.NewDefaultDelegate(), 0, 25)
				m.listModel.Title = "Tag List"
				updateDims(Listing)
				var cmd tea.Cmd
				m.listModel, cmd = m.listModel.Update(msg)
				return m, cmd
			}
		}
		// TODO: Handle returning to Typing without accepting first
	case Listing:
		switch msg := msg.(type) {
		case tea.KeyMsg:
			switch msg.Type {
			case tea.KeyCtrlC:
				return m, tea.Quit
			case tea.KeyEnter:
				img := m.Images[m.listModel.Index()]
				m.RequestedImageName = fmt.Sprintf("%s:%s", img.Name, img.Tag)
				m.state = Prompting
				m.textInput.Placeholder = "[y/n]"
				m.textInput.Focus()
				updateDims(Prompting)
				return m, nil
			}
		case tea.WindowSizeMsg:
			updateDims(Listing)
			/*
				physicalWidth, physicalHeight, _ := term.GetSize(int(os.Stdout.Fd()))
				w := docStyle.GetWidth()
				h := docStyle.GetHeight()
				docStyle.Margin((physicalHeight-h)/4, (physicalWidth-w)/2)
				docStyle.PaddingTop(physicalHeight / 8)
			*/
			m.listModel.SetSize(0, len(m.listModel.Items()))
			var cmd tea.Cmd
			m.listModel, cmd = m.listModel.Update(msg)
			return m, cmd
		}
	case Prompting:
		switch msg := msg.(type) {
		case tea.WindowSizeMsg:
			updateDims(Prompting)
		case tea.KeyMsg:
			switch msg.Type {
			case tea.KeyCtrlC:
				return m, tea.Quit
			case tea.KeyEnter:
				val := strings.TrimSpace(strings.ToLower(m.textInput.Value()))
				switch val {
				case "n", "no":
					m.state = Typing
					updateDims(Typing)
					m.textInput.Reset()
					m.textInput.Placeholder = m.DefaultImageName
					return m, nil
				case "y", "yes":
					m.state = Downloading
					updateDims(Downloading)
					m.AcceptedImageName = m.RequestedImageName
					return m, tea.Batch(
						spinner.Tick,
						m.fetch(),
					)
				}
			}
		}
	case Downloading:
		switch msg := msg.(type) {
		case tea.WindowSizeMsg:
			updateDims(Downloading)
		case tea.KeyMsg:
			switch msg.Type {
			case tea.KeyCtrlC:
				return m, tea.Quit
			}
		case types.Image:
			updateDims(Results)
			m.err = msg.Err
			m.state = Results
			m.textInput.Reset()
			m.textInput.Placeholder = ""
			return m, nil
		}
	case Results:
		switch msg.(type) {
		case tea.WindowSizeMsg:
			updateDims(Results)
		case tea.KeyMsg:
			return m, tea.Quit
		}
	}

	var cmd tea.Cmd

	switch m.state {
	case Typing:
		m.textInput, cmd = m.textInput.Update(msg)
	case Listing:
		m.listModel, cmd = m.listModel.Update(msg)
	case Prompting:
		m.textInput, cmd = m.textInput.Update(msg)
	case Downloading:
		m.spinner, cmd = m.spinner.Update(msg)
	case Results:
		m.textInput, cmd = m.textInput.Update(msg)
	}

	return m, cmd
}

// We need to manually convert from `types.ImageList` to `[]list.Item` because
//   Golang doesn't allow native conversions that hide complexity.  This will
//   always be O(n) and there's nothing we can do about it.
func (m *Model) list(s string) ([]list.Item, error) {
	l, e := m.Lister(s)
	if e != nil {
		return nil, e
	}
	m.Images = l
	items := make([]list.Item, len(l))
	for i := range l {
		items[i] = l[i]
	}
	return items, nil
}

func (m *Model) fetch() tea.Cmd {
	return func() tea.Msg {
		n, e := m.Fetcher(m.AcceptedImageName)
		return types.Image{
			Name: n,
			Err:  e,
		}
	}
}

func updateDims(s ModelState) {
	physicalWidth, physicalHeight, _ := term.GetSize(int(os.Stdout.Fd()))
	w := docStyle.GetWidth()
	h := docStyle.GetHeight()
	docStyle.Margin((physicalHeight-h)/4, (physicalWidth-w)/2)
	docStyle.Margin((physicalHeight-h)/4, (physicalWidth-w)/2)

	switch s {
	case Listing:
		// The list model is a little taller so we use less padding
		docStyle.PaddingTop(physicalHeight / 8)
	default:
		docStyle.PaddingTop(physicalHeight / 4)
	}
}
