/**
 * File: model.go
 * Written by: Stephen M. Reaves
 * Created on: Fri, 11 Mar 2022
 */

// TODO: Listing seems to be weird?
package tui

import (
	"os"

	"github.com/charmbracelet/bubbles/list"
	"github.com/charmbracelet/bubbles/spinner"
	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"gitlab.com/reavessm/obi/pkg/container"
	"gitlab.com/reavessm/obi/pkg/types"
	"golang.org/x/term"
)

func ShowAndRun(m *Model) error {
	return tea.NewProgram(m, tea.WithAltScreen()).Start()
}

type ModelState int

const (
	Typing ModelState = iota
	Listing
	Prompting
	Downloading
	Exporting
	Untarring
	Results
)

type Model struct {
	Fetcher types.FetcherFunc
	Lister  types.ListerFunc

	DefaultImageName   string
	AcceptedImageName  string
	RequestedImageName string

	SelectedImage types.Image
	Images        types.ImageList

	spinner   spinner.Model
	textInput textinput.Model
	listModel list.Model
	choice    string

	state ModelState

	err error
}

var docStyle = lipgloss.
	NewStyle().
	Width(80).
	Height(40).
	Align(lipgloss.Center).
	BorderStyle(lipgloss.RoundedBorder()).
	BorderForeground(lipgloss.Color("#874BFD"))

func NewModel(args ...string) *Model {
	defaultImg := container.DefaultImage
	if len(args) > 0 {
		defaultImg = args[0]
	}

	t := textinput.NewModel()
	t.Placeholder = defaultImg
	t.PromptStyle = t.PromptStyle.Width(2)
	t.Focus()

	s := spinner.NewModel()
	s.Spinner = spinner.MiniDot

	physicalWidth, physicalHeight, _ := term.GetSize(int(os.Stdout.Fd()))
	w := docStyle.GetWidth()
	h := docStyle.GetHeight()
	docStyle.Margin((physicalHeight-h)/4, (physicalWidth-w)/2)
	docStyle.PaddingTop(physicalHeight / 4)

	return &Model{
		textInput:        t,
		spinner:          s,
		state:            Typing,
		DefaultImageName: defaultImg,
	}
}

func (m *Model) Init() tea.Cmd {
	switch m.state {
	case Typing:
		return textinput.Blink
	default:
		return nil
	}
}
