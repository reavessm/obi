/**
 * File: view.go
 * Written by: Stephen M. Reaves
 * Created on: Fri, 11 Mar 2022
 */

package tui

import "fmt"

func (m *Model) View() string {
	switch m.state {
	case Typing:
		return docStyle.Render(
			fmt.Sprintf(
				"Enter image name:\n\n%s\n\n%s",
				m.textInput.View(),
				"(Press ctrl+c to quit)",
			),
		)
	case Listing:
		return docStyle.Render(
			m.listModel.View(),
		)
	case Prompting:
		return docStyle.Render(
			fmt.Sprintf(
				"Are you sure you want to install \n\n'%s'?\n\n%s",
				m.RequestedImageName,
				m.textInput.View(),
			),
		)
	case Downloading:
		return docStyle.Render(
			fmt.Sprintf(
				"Downloading and extracting image,\n\nplease wait %s",
				m.spinner.View(),
			),
		)
	case Results:
		if m.err != nil {
			return docStyle.Render(
				fmt.Sprintf(
					"Error!  Could not install %s!\n\n%v\n\n%s%s",
					m.AcceptedImageName,
					m.err,
					"Press any key to quit",
					m.textInput.View(),
				),
			)
		}
		return docStyle.Render(
			fmt.Sprintf(
				"Finished installing \n\n%s!\n\n%s\n\n%s",
				m.AcceptedImageName,
				"Press any key to quit",
				m.textInput.View(),
			),
		)
	}

	if err := m.err; err != nil {
		return fmt.Sprintf("Could not fetch image: %v", err)
	}

	return "Thanks for playing along!"
}
